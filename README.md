# Spring Cloud Demo

## Modules

### config-server

- This module has been used to setup a local config server.
- You will need to copy configuration files from `/config` directory and perform following operations:
```
git init . 
git add --all
git commit -m "Config Directory commit"
```
- Update property `spring.cloud.config.server.git.uri` with your local configuration files directory.
- Property `spring.cloud.config.server.git.force-pull` will be used to auto-pick the configuration files if any changes done.
- Also note, we must use `@EnableConfigServer` annotation in our main application class.

### guest-services

- This is a client module for guests, which has end-points to GET guests information.

### reservation-services

- This is a client module for reservation, which has end-points to GET reservation details.


### room-services

- This is a client module for rooms, which has end-points to GET rooms information.


### eureka-services

- This module has been used as a eureka-server.
- In order to create a eureka-server, you would need to add following dependency in your module:
```
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
		</dependency>
```
- You will also need your main application class to annotate with `@EnableEurekaServer`.

### room-reservation-services

- I have used this module to demonstrate the use of `FeignClient`.
- Below dependency is required to use `FeignClient`:
```
  	<dependency>
  		<groupId>org.springframework.cloud</groupId>
  		<artifactId>spring-cloud-starter-openfeign</artifactId>
  	</dependency>
```
- Annotation used: `@EnableFeignClients`


## Annotations used throughout the application:

- @SpringBootApplication : The `@SpringBootApplication` annotation is equivalent to using `@Configuration`, `@EnableAutoConfiguration` and `@ComponentScan`.
- @RestController : `@RestController` is used to create RESTful web services, it provides annotations like @Controller and @ResponseBody implicitly.
- @RequestMapping  : The `RequestMapping` is used to map web requests to the controller methods.
- @EnableFeignClients : Scans for interfaces that declare they are feign clients (via `@FeignClient`).
- @EnableEurekaServer : The `@EnableEurekaServer` annotation is used to make your Spring Boot application acts as a Eureka Server.
- @EnableDiscoveryClient : `@EnableDiscoveryClient` annotation can work with any Discovery Client implementations which implements in your project( Eureka, Consul, Zookeeper ).
- @FeignClient: Annotation for interfaces declaring that a REST client with that interface should be created (e.g. for autowiring into another component).
- @Repository: `@Repository` annotates classes at the persistence layer, which will act as a database repository
