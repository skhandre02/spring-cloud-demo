package com.swapnil.se.webservices;

import com.swapnil.se.entity.Guest;
import com.swapnil.se.repositories.GuestRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/guests")
public class GuestWebService {
    private final GuestRepository repository;

    public GuestWebService(GuestRepository repository) {super();this.repository=repository;}

    @GetMapping
    Iterable<Guest> getAllGuests() {return repository.findAll();}


    @GetMapping("/{id}")
    Guest getGuest(@PathVariable("id") long id) {return repository.findById(id).get();}
}
